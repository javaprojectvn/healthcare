package com.canh.healthcare.domain.impl;

import static net.sf.dynamicreports.report.builder.DynamicReports.stl;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.canh.healthcare.domain.interfaces.PatientBillBusiness;
import com.canh.healthcare.domain.interfaces.PatientBusiness;
import com.canh.healthcare.jpa.entity.Medicine;
import com.canh.healthcare.jpa.entity.PatientBill;
import com.canh.healthcare.jpa.entity.PatientBillDetails;
import com.canh.healthcare.model.PatientBillDetailsDto;
import com.canh.healthcare.model.PatientBillDto;
import com.canh.healthcare.model.PatientDto;
import com.canh.healthcare.services.impl.PatientBillServiceImpl;
import com.canh.healthcare.services.interfaces.PatientBillService;
import com.canh.healthcare.utils.ResultInfo;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.column.Columns;
import net.sf.dynamicreports.report.builder.component.Components;
import net.sf.dynamicreports.report.builder.datatype.DataTypes;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class PatientBillBusinessImpl implements PatientBillBusiness {
	PatientBillService service = PatientBillServiceImpl.getInstance();
	PatientBusiness patientServcie = new PatientBusinessImpl();

	@Override
	public void create(PatientBillDto patientBillDto, PatientDto patientDto) {
		PatientBill patientBill = convertToPatientBill(patientBillDto);
		// int patientBillId = service.create(patientBill);
		ResultInfo resultInfo = service.create(patientBill);
		int patientBillId = ((PatientBill) resultInfo.getObject()).getPatientBillId();
		for (int i = 0; i < patientDto.getPattientRecords().size(); i++) {
			patientDto.getPattientRecords().get(i).setPatientBillId(patientBillId);

		}
		patientServcie.update(patientDto);

	}

	@Override
	public void update(PatientBillDto patientBilDto) {
		// TODO Auto-generated method stub

	}

	@Override
	public PatientBillDto searchPatientBillById(int id) {
		PatientBill patientBill = service.findById(id);
		return convertToPatientBillDto(patientBill);
	}

	public static PatientBill convertToPatientBill(PatientBillDto patientBillDto) {
		PatientBill patientBill = new PatientBill(patientBillDto);
		List<PatientBillDetails> patientBillDetailList = new ArrayList<PatientBillDetails>();
		for (PatientBillDetailsDto patientBillDetailDto : patientBillDto.getPatientBillDetails()) {
			PatientBillDetails patientBillDetail = convertToPatientBillDetail(patientBillDetailDto);
			patientBillDetailList.add(patientBillDetail);
		}
		patientBill.setPatientBillDetails(patientBillDetailList);
		return patientBill;
	}

	public static PatientBillDetails convertToPatientBillDetail(PatientBillDetailsDto patientDetailDto) {
		PatientBillDetails billDetail = new PatientBillDetails();
		Medicine medicine = new Medicine(patientDetailDto.getMedicine());
		PatientBill patientBill = new PatientBill();
		billDetail.setMedicine(medicine);
		billDetail.setPatientBill(patientBill);
		billDetail.setQuantity(patientDetailDto.getQuantity());
		return billDetail;

	}

	public static PatientBillDetailsDto convertToPatientBillDetailDto(PatientBillDetails patientBillDetail) {
		PatientBillDetailsDto patientBillDetailsDto = new PatientBillDetailsDto();
		patientBillDetailsDto.setQuantity(patientBillDetail.getQuantity());
		// patientBillDetailsDto.setPatientBill(PatientBillBusinessImpl.convertToPatientBillDto(patientBillDetail.getPatientBill()));
		patientBillDetailsDto.setMedicine(MedicineBusinessImpl.convertToMedicineDto(patientBillDetail.getMedicine()));
		return patientBillDetailsDto;

	}

	public static PatientBillDto convertToPatientBillDto(PatientBill patientBill) {
		PatientBillDto patientBillDto = new PatientBillDto();
		patientBillDto.setCreateDate(patientBill.getCreateDate());
		patientBillDto.setPatientBillId(patientBill.getPatientBillId());
		patientBillDto.setPatient(PatientBusinessImpl.convertToPatientDto(patientBill.getPatient()));
		List<PatientBillDetailsDto> patientBillDetailDtoLst = new ArrayList<PatientBillDetailsDto>();
		for (PatientBillDetails patientBillDetail : patientBill.getPatientBillDetails()) {
			PatientBillDetailsDto patientBillDetailsDto = convertToPatientBillDetailDto(patientBillDetail);
			patientBillDetailDtoLst.add(patientBillDetailsDto);
		}
		patientBillDto.setPatientBillDetails(patientBillDetailDtoLst);
		return patientBillDto;
	}

	public void createReport(PatientDto patientDto, PatientBillDto patientBill) {
		JasperReportBuilder report = DynamicReports.report();// a new report
		List<String> a = new ArrayList<String>();
		StyleBuilder boldStyle = stl.style().bold();
		StyleBuilder boldCenteredStyle = stl.style(boldStyle).setHorizontalAlignment(HorizontalAlignment.CENTER);
		StyleBuilder columnTitleStyle = stl.style(boldCenteredStyle).setBorder(stl.pen1Point())
				.setBackgroundColor(Color.LIGHT_GRAY);

		report.setColumnTitleStyle(columnTitleStyle).highlightDetailEvenRows();
		report.columns(Columns.column("Tên thuốc", "name", DataTypes.stringType()),
				Columns.column("Số lượng", "quantity", DataTypes.integerType()),
				Columns.column("Liều dùng", "description", DataTypes.stringType()))
				// Columns.column("Date", "date", DataTypes.dateType()))
				.title(// title of the report
						Components.text("PHÒNG KHÁM CHUYÊN KHOA TÂM THẦN")
								.setHorizontalAlignment(HorizontalAlignment.CENTER).setStyle(boldCenteredStyle))
				.title(// title of the report
						Components.text("TEST REPORT").setHorizontalAlignment(HorizontalAlignment.CENTER).setStyle(boldCenteredStyle))
				.title(// title of the report
						Components.text("Tân bình, TP.Hồ CHí Minh")
								.setHorizontalAlignment(HorizontalAlignment.CENTER))
				.title(// title of the report
						Components.text("ĐT: 123456789").setHorizontalAlignment(HorizontalAlignment.CENTER))
				.addTitle(Components.line().setPen(stl.pen1Point()))
				.addTitle(Components.text("TOA THUỐC").setHorizontalAlignment(HorizontalAlignment.CENTER).setStyle(boldCenteredStyle))
				.addTitle(Components.text("Họ tên:" + patientDto.getName() + "")
						.setHorizontalAlignment(HorizontalAlignment.LEFT))
				.addTitle(Components.text("Địa chỉ:" + patientDto.getAddress() + "")
						.setHorizontalAlignment(HorizontalAlignment.LEFT))
				.addTitle(Components.text("Chẩn đoán:")
						.setHorizontalAlignment(HorizontalAlignment.LEFT))
				.addTitle(Components.line().setPen(stl.pen1Point()))
				.title(Components.text("  "))
				.pageFooter(Components.pageXofY())// show page number on the page foote
				.pageFooter(Components.currentDate().setHorizontalAlignment(HorizontalAlignment.RIGHT))
				// .setDataSource(new JREmptyDataSource());
				.setDataSource(createDataSource(patientBill));

		try {
			// show the report
			report.show();

			// export the report to a pdf file
			report.toPdf(new FileOutputStream("D:/report.pdf"));
		} catch (DRException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private JRDataSource createDataSource(PatientBillDto patientBill) {
		List<ReportBillData> reportBillDataLst = new ArrayList<ReportBillData>();
		for (PatientBillDetailsDto patientBillDetail : patientBill.getPatientBillDetails()) {
			ReportBillData reportBillData = new ReportBillData();
			String medicineName = patientBillDetail.getMedicine().getName();
			int quantity = patientBillDetail.getQuantity();
			String description = "1-1-1";
			// Object[] row = { id, examinationDate, medicineName, quantity, description };
			reportBillData.setName(medicineName);
			reportBillData.setDescription(description);
			reportBillData.setQuantity(quantity);
			reportBillDataLst.add(reportBillData);
		}
		return new JRBeanCollectionDataSource(reportBillDataLst);
	}

	// Nest class to export pdf file
	public class ReportBillData {
		String name;
		int quantity;
		String description;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getQuantity() {
			return quantity;
		}

		public void setQuantity(int quantity) {
			this.quantity = quantity;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

	}

}
