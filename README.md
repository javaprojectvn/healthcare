# HealhCare setup

### Environment:
  - Jdk >=1.7
  - Maven >=3.x
  - PostgressSQL 9.3
 
### Installation:
```sh
git clone git@bitbucket.org:javaprojectvn/healthcare.git
cd healthcare\HealthCare\
mvn clean install # to download all dependencies
mvn eclispe:eclipse # create eclipse project
```

#### first run:
```xml
<!--create db with name TestDb in Postgress
Update file persistence.xml as bellow:!-->
<!--change --> 
<property name="hibernate.hbm2ddl.auto" value="update" />  
<!--to -->
<property name="hibernate.hbm2ddl.auto" value="create-drop" /> <!--#using to create table-->
<!--and  next, run application with file App.java and finally change: -->
<property name="hibernate.hbm2ddl.auto" value="create-drop" /> 
<!--to-->  
<property name="hibernate.hbm2ddl.auto" value="update" />
```
#### Feature:
- Quản lý bệnh án, thông tin bệnh nhân và toa thuốc
- Quản lý về xuất nhập thuốc men
- Quản lý về thu chi của pk (tính tiền toa thuốc cho BN, tiền khám và tiền thuốc thu vào, tiền mua thuốc...)
#### Story:
> Khi 1 Bn mới đến, thư ký bên ngoài sẽ gõ thông tin Bn như họ tên, tuổi, địa chỉ, số đt... vào chương trình.

> Bn vào phòng khám, chị ngồi khám cho BN, vừa hỏi vừa gõ bệnh án, sau đó ra toa thuốc, tự động số tiền thuốc được tính ra luôn, rồi chuyển toa ra ngoài cho thư ký in ra đưa BN và ghi hoá đơn.

> Khi BN đó tái khám, chị mở lại bệnh án để xem thông tin bênh tật của họ, điền thêm diễn tiến của ngày tái khám, mở toa thuốc cũ ra xem và điều chỉnh để thành toa mới

>Tức là chị mong muốn trong ngoài kết hợp được với nhau nhưng người thư ký bên ngoài sẽ ko thấy được những phần quản lý về thuốc men và thu chi của pk cũng như không can thiệp sâu được.

### Tech:
* [Swing] - create GUI
* [JPA-Hibernate] - for accessing, persisting, and managing data between Java objects / classes and a relational database
* [PostgressSql] - database
* [Maven] - Build project


### Development
Using git to manage version, with new feature create new branch to implement:
```sh
 git checkout -b [newbranch]
 # after finish, push code to bitbuket
 git push origin [remote brang]
```
Unit test if necessary

#### Building for source
```sh
mvn clean install #in case update pom file
mvn install
mvn package # build java file only 
```

### Todos
 - Comming soon!

License
----

MIT


**Free Software, Hell Yeah!**
